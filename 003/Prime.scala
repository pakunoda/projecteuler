object Prime {
  def main(args: Array[String]) = {
    var count = 1L
    var list: List[Long] = Nil
    val number = 600851475143L
    while(count < number / 2){
      if(number % count == 0 && isPrime(count)) {
        println("found " + count)
        list = count :: list
      }
      count += 1
    }
    println(list)
  }

  def isPrime(i: Long) = (for(j <- 1.toLong until i if i % j == 0) yield j).size == 1
}
