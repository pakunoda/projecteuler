object Problem {
  def main(args: Array[String]) {
    val numbers = scala.io.Source.fromFile("cipher1.txt").getLines().toList.head.split(',').map(x => x.toInt)
    val firstSet = (97 to 122).filter(x => (0 until numbers.size by 3).map(i => isValid((x ^ numbers(i)).toChar)).reduce(_&&_))
    val secondSet = (97 to 122).filter(x => (1 until numbers.size by 3).map(i => isValid((x ^ numbers(i)).toChar)).reduce(_&&_))
    val thirdSet = (97 to 122).filter(x => (2 until numbers.size by 3).map(i => isValid((x ^ numbers(i)).toChar)).reduce(_&&_))

    for {
      i <- firstSet
      j <- secondSet
      k <- thirdSet
      decryptedText = decrypt(i, j, k, numbers)
      if decryptedText.contains(" and ")
    } {
      println(decryptedText)
      println("score " + decryptedText.map(x => x.toInt).sum.toInt)
    }
  }

  def isValid(n: Char) = !n.isControl
  def decrypt(a: Int, b: Int, c: Int, xs: Array[Int]) = xs.zipWithIndex.map { x => x match {
    case x if x._2 % 3 == 2 => (x._1 ^ c, x._2)
    case x if x._2 % 3 == 1 => (x._1 ^ b, x._2)
    case x => (x._1 ^ a, x._2)
  }}.map(x => x._1.toChar).mkString
}
