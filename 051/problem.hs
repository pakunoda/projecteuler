import Data.List
import Data.Char
import Control.Monad.ST
import Data.Array.ST
import Data.Array.Unboxed
import Control.Monad

main = print . head . dropWhile f $ primes
  where p = prime 1000000
        primes = [x|x <- [50000..1000000], p ! x == 1]
        f x = not . foldl (||) False $ combinations (length stringified) >>= \combo -> do
          let permutes = map (\y -> replaceNumAtIndices stringified (intToDigit y) combo) $ [0..9]
              matchingPrimes = nub . filter (\y -> p ! (read y) == 1 && head y /= '0') $ permutes
              matchingPrimesCount = length matchingPrimes
              containsPrime = show x `elem` matchingPrimes
          [matchingPrimesCount == 8 && containsPrime]
          where stringified = show x

replaceNumAtIndices x num indices = map (\y -> f y) $ zip x [0..]
  where f y = if snd y `elem` indices then num else fst y

combinations len = filter (\x -> length x == 3) $ subsequences [0..len - 1]

prime :: Int -> UArray Int Int
prime n = runSTUArray $ do
  arr <- newArray ( 0 , n ) 1 :: ST s ( STUArray s Int Int )
  writeArray arr 0 0
  writeArray arr 1 0
  forM_ ( takeWhile ( \x -> x*x <= n ) [ 2 .. n ] ) $ \i -> do
    ai <- readArray arr i
    when ( ai == 1 ) $ forM_ [ i^2 , i^2 + i .. n ] $ \j -> do
      writeArray arr j 0
  return arr
