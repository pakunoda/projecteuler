import scala.annotation.tailrec
import scala.collection.mutable.HashMap

object Problem {

  def main(arg: Array[String]) {
    var cache = new HashMap[BigInt, BigInt]

    for(i <- BigInt(13) until 1000000) {
      var x = i
      var internalCache = Vector.empty[BigInt]
      while (x > 1) {
        internalCache = internalCache :+ x
        if (!cache.contains(x)) {
          if (x % 2 == 0) x = x / 2
          else x = 3 * x + 1
        }
        else x = 1
      }
      val last = cache.get(internalCache.last).getOrElse(BigInt(1))
      internalCache.zip(internalCache.size to 1 by -1).foreach(x => cache += x._1 -> (x._2 + last))
    }

    println(cache.max(Ordering[BigInt].on[(_,BigInt)](_._2)))
  }

}
