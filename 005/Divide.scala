object Divide {
  def main(args: Array[String]) {
    println(smallestDivisible(2520))
  }

  def smallestDivisible(num: Int): Int = {
    if((3 to 20).dropWhile(num%_==0).size == 0) num
    else smallestDivisible(num + 20)
  }
}
