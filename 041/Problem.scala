import scala.math._
def f(a: Int, b: Int) = if(isPrime(b) && b > a) b else a
def g(a: Int) = (1 to a).foldLeft("")((a,b) => a + b)
println((for(i <- 1 to 9) yield g(i).permutations).map(x => x.foldLeft(0)((a,b) => f(a, b.toInt) )))

def isPrime(i: Int): Boolean = {
  if(i % 2 == 0) false
  else isPrime(3, sqrt(i).toInt, i)
}

def isPrime(a: Int, b: Int, c: Int): Boolean = {
  if(a > b) true
  else if (c % a == 0) false
  else isPrime(a + 2, b, c)
}
