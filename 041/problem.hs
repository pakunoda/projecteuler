import Data.List

main = print $ map (\x -> foldl (\a b -> f a (read b)) 1 x) $ perms 7
  where f a b = if isPrime b && b > a then b else a
        perms i = map (\x -> permutations $ foldl (\a b -> a ++ show b) "" [1..x]) [1..i]

isPrime a
  | a `mod` 2 == 0 = False
  | otherwise = isPrime' 3 (floor . sqrt . fromIntegral $ a) a

isPrime' a b c
  | a > b = True
  | c `mod` a == 0 = False
  | otherwise = isPrime' (a + 2) b c
