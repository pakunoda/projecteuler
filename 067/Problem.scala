import scala.io._

object Problem {

  def main(args: Array[String]) {
    val list = Source.fromFile("triangle.txt").getLines().map(x => x.split(" ").filter(_.size > 0).map(BigInt(_)).toArray).toArray

    for (i <- list.size - 2 to 0 by -1; j <- 0 until list(i).size) {
      val value = if (list(i + 1)(j) > list(i + 1)(j + 1)) list(i + 1)(j) else list(i + 1)(j + 1)
      list(i)(j) += value
    }
    println(list(0)(0))
  }

}
