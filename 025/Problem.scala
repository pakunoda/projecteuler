object Problem {
  def main(args: Array[String]) {
    var count = BigInt(2)
    var p = BigInt(1)
    var c = BigInt(1)
    while(c.toString.size < 1000) {
      c = p + c
      p = c - p
      count += 1
    }
    println(count)
  }
}
