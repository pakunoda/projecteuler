val pentagonal = (1 to 5000).map(x => x * (3 * x - 1) / 2)
val lookup = pentagonal.zipWithIndex.toMap
def f(x:Int, y: Int) = lookup.contains(x - y) && lookup.contains(x + y)

println(pentagonal.par.map(x => pentagonal.filter(y => f(x,y)).map(y => (y, x))).flatten)
