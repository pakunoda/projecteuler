import qualified Data.Map as M

main = print $ a >>= \x -> map (\y -> (y, x)) . filter (\y -> f y x) $ a
  where f x y = (M.member (x-y) p) && (M.member (x+y) p)
        p = M.fromList $ zip a [1..]
        a = pentagonal 5000

pentagonal n = map (\x -> floor $ x * (3 * x - 1) / 2)  [1..n]
