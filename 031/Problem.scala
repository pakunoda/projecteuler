object Problem {
  def main(args: Array[String]) {
    /* Terrible solution */
    val test = for {
      i <- 0 to 200
      j <- 0 to 200 by 2
      k <- 0 to 200 by 5
      l <- 0 to 200 by 10
      m <- 0 to 200 by 20
      o <- 0 to 200 by 50
      p <- 0 to 200 by 100
      q <- 0 to 200 by 200
      x = i + j + k + l + m + o + p + q
      if(x == 200)
    } yield x

    println(test.size)
  }
}
