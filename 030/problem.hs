import Data.Char

main = do
  let lookup = [0, 1, 32, 243, 1024, 3125, 7776, 16807, 32768, 59049]
  let a = filter (\x -> fst x == snd x) [(sum [lookup !! digitToInt z | z <- show(x)], x) | x <- [32..354294]]
  let b = sum $ map fst a
  print b
