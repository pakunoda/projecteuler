object Problem {
  def main(args: Array[String]) {
    var lookup = Array(0, 1, 32, 243, 1024, 3125, 7776, 16807, 32768, 59049)
    val a = (32 to 354294).filter(x => x.toString.map(x => lookup(x.toString.toInt)).sum == x).sum
    println("solution - " + a)
  }
}
