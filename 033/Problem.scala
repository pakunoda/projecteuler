import scala.annotation.tailrec

object Problem {
  def main(args: Array[String]) {
    val t = for{i <- 10 to 99; j <- 10 to 99 if f(i, j)} yield (BigInt(i), BigInt(j))
    val u = t.reduce((x,y) => (x._1 * y._1, x._2 * y._2))
    println(g(u._1, u._2))
  }

  @tailrec def g(i: BigInt, j: BigInt): (BigInt, BigInt) = {
    val d = i.gcd(j)
    if(d == 1) (i, j)
    else g(i / d, j / d)
  }

  def f(i: Int, j: Int) = {
    val x = i.toString
    val y = j.toString

    if((x.last != '0' || y.head != '0') && x.last == y.head) {
      val xa = x.head.toString.toDouble
      val ya = y.last.toString.toDouble
      xa / ya == i.toDouble / j && x != y && x.head != x.last && y.head != y.last
    }
    else false
  }
}
