import qualified Data.Set as Set

main = do
  let a = Set.fromList [x^y | x <- [2..100], y <- [2..100]]
  print $ show $ Set.size(a)
