import scala.collection.immutable.Set
import scala.math._

object Problem {
  def main(args: Array[String]) {
    val t = (for {i <- 2 to 100; j <- 2 to 100} yield pow(i, j)).toSet
    println(t.size)
  }
}
