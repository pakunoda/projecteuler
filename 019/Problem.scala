import scala.collection.mutable.ArrayBuffer

object Problem {
  def main(args: Array[String]) {
    val days = new ArrayBuffer[Int]

    for (i <- 1 until 101) {
      val a = f(i)(_)
      val b = g(i)(_)
      val c = odd(i)(_)

      a.andThen(c).andThen(a).andThen(b).andThen(a).andThen(b).andThen(a).andThen(a).andThen(b).andThen(a).andThen(b).andThen(a)(days)
    }
    println(days.drop(5).grouped(7).filter(_(0) == 1).size)
  }

  def f(y: Int)(a: ArrayBuffer[Int]) = a ++= 1 to 31
  def g(y: Int)(a: ArrayBuffer[Int]) = a ++= 1 to 30
  def odd(y: Int)(a: ArrayBuffer[Int]) = if(y%4==0) a ++= (1 to 29) else a ++= (1 to 28)
}
