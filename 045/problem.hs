import Debug.Trace

main = do
  let a t = quad 0.5 0.5 t
  let b p = quad (3.0/2.0) (-0.5) p
  let c h = quad 2 (-1) h
  let r = [x | x <- [50000..], let ax = a(negate(x)), let bx = b(negate(x)), let cx = c(negate(x)), ax, bx, cx]
  print $ take 1 r

quad a b c
  | isInt(x) || isInt(y) = True
  | otherwise = False
  where x = (negate(b) + sqrt((b^2) - 4*a*c)) / (2 * a)
        y = (negate(b) - sqrt((b^2) - 4*a*c)) / (2 * a)

isInt x = x == fromInteger (round x)
