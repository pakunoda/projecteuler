import Data.List
import qualified Data.Set as S

main = do
  let values = ["0", "1", "2", "3", "4", "5", "X", "7", "8", "X"]
      combos = combinations 6 values >>= \firstDice -> do
        combinations 6 values >>= \secondDice -> do
          let firstSet = foldl (\result x -> S.union result (procureCombo x secondDice)) S.empty $ firstDice
              finalSet = foldl (\result x -> S.union result (procureCombo x firstDice)) firstSet $ secondDice
          [S.foldl (\result x -> result && S.member x finalSet) True squares]
  print . div 2 . length . filter (\x -> x) $ combos

squares = S.fromList ["01", "04", "0X", "1X", "25", "3X", "4X", "X4", "81"]
combinations n x = filter ((n==).length) . subsequences $ x
procureCombo number dice = S.fromList . filter(\x -> S.member x squares) . map (\x -> number ++ x) $ dice
