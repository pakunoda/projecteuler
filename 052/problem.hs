import Data.List

main = print . take 1 . dropWhile (\x -> f x) $ [125874..]
  where isEquivalent x y = (sort . show $ x) == (sort . show $ y)
        f x
          | (head (show x)) == '1' = length (dropWhile (\y -> isEquivalent x (y * x)) [2..6]) > 0
          | otherwise = True
