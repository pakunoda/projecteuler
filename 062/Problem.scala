import scala.collection.mutable.HashMap

object Problem {

  val cubes = {
    def loop(n: BigDecimal): Stream[BigDecimal] = (n * n * n) #:: loop(n + 1)
    loop(1)
  }

  def main(args: Array[String]) {
    val intermediates = new HashMap[String, (BigDecimal, Int)]
    cubes.dropWhile { cube =>
      val base = cube.toString.sortBy(x => x)
      if (!intermediates.contains(base)) intermediates += ((base, (cube, 0)))

      intermediates(base) = (intermediates(base)._1, intermediates(base)._2 + 1)
      val result = intermediates(base)._2 < 5
      if (!result) println(intermediates(base)._1)
      result
    }
  }

}
