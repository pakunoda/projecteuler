import Data.List

main = print . sum $ [1..100] >>= \n -> [0..n] >>= \r -> [combination n r]
  where combination n r
          | product [1..n] / (product [1..r] * product [1..(n - r)]) > 1000000 = 1
          | otherwise = 0
