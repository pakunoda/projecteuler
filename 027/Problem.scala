import scala.collection.mutable.{ArrayBuffer, HashMap}

object Problem {
  def main(args:Array[String]) {
    val q = primes(1000000)
    val p = q.view.zipWithIndex.toMap
    def quad(n: BigInt, a: BigInt, b: BigInt) = n.pow(2) + (a * n) + b
    var largestA = BigInt(0)
    var largestB = BigInt(0)
    var largestN = BigInt(0)

    for { a <- (BigInt(-1000) to 1000); b <- (BigInt(-1000) to 1000) } {
      var count = BigInt(0)
      while(p.contains(quad(count, a, b))) count += 1

      if(count > largestN) {
        largestN = count
        largestA = a
        largestB = b
      }
    }
    println("a " + largestA + " b " + largestB)
    println(largestA * largestB)
  }

  def primes(n: BigInt) = {
    val x = n + 1

    val sieve = new ArrayBuffer[BigInt]

    sieve += (0, 0)
    var count = 2
    while(count < x) {
      sieve += count
      count += 1
    }

    for(i <- BigInt(2) until(scala.math.pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i.toInt) != 0) {
        for(j <- scala.math.pow(i.toInt, 2).toInt until x.toInt by i.toInt) sieve(j.toInt) = 0
      }
    }
    sieve.filter(y => y != 0 && y < x)
  }
}
