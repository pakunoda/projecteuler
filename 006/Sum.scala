object Sum {
  def main(args: Array[String]) {
    val squaresum = (BigInt(1) to 100).sum * (BigInt(1) to 100).sum
    println(squaresum)
    val sumsquare = (BigInt(1) to 100).map(x => x * x).sum
    println(sumsquare)
    println(sumsquare - squaresum)
  }
}
