import scala.collection.mutable.ArrayBuffer

object Problem {

  val p = primes(750000)
  val pIndexed = p.zipWithIndex.toMap

  def main(args: Array[String]) {
    val x = for(x <- p if isTruncatablePrime(x.toString, i => i.take(i.size - 1)) && isTruncatablePrime(x.toString, _.drop(1))) yield x
    println(x.filter(x => x > 10).sum)
  }

  def isTruncatablePrime(i: String, f: String => String): Boolean = {
    if(i.size == 0) true
    else if(i.size == 1 && pIndexed.contains(BigInt(i))) true
    else if(i.size == 1) false
    else if(!pIndexed.contains(BigInt(i))) false
    else isTruncatablePrime(f(i), f)
  }

  def primes(n: BigInt): ArrayBuffer[BigInt] = {
    val x = n + 1
    val sieve = new ArrayBuffer[BigInt]
    sieve += (0, 0)
    for(i <- 2 until x.toInt) sieve += i

    for(i <- BigInt(2) until(scala.math.pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i.toInt) != 0) {
        for(j <- scala.math.pow(i.toInt, 2).toInt until x.toInt by i.toInt) sieve(j.toInt) = 0
      }
    }
    sieve.filter(y => y != 0 && y < x)
  }
}

