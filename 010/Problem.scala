import scala.collection.mutable.ArrayBuffer

object Problem {

  val sieve = new ArrayBuffer[BigInt]
  def main(args: Array[String]) {
    val t1 = System.currentTimeMillis()
    val list = primes(2000000)
    val t2 = System.currentTimeMillis()

    println("Time " + (t2 - t1))
  }

  def primes(n: BigInt) = {
    val x = n + 1

    if(sieve.size == 0) {
      sieve += (0, 0)
      var count = 2
      while(count < x) {
        sieve += count
        count += 1
      }
    }
    else if(sieve.size < x) {
      for(i <- sieve.size until x.toInt) sieve += i
    }

    for(i <- BigInt(2) until(scala.math.pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i.toInt) != 0) {
        for(j <- scala.math.pow(i.toInt, 2).toInt until x.toInt by i.toInt) sieve(j.toInt) = 0
      }
    }
    sieve.filter(y => y != 0 && y < x)
  }
}
