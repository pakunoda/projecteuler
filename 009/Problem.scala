import scala.math._

object Problem {
  def main(args: Array[String]) {
    for {
      i <- 0 until 1000
      j <- (i + 1) until 1000
    } {
      val a = pow(i, 2)
      val b = pow(j, 2)
      val c = pow(1000 - i - j, 2)
      if(a + b == c) {
        println("a " + i + " b " + j + " c " + (1000 - i - j))
        println(i * j * (1000 - i - j))
      }
    }
  }
}
