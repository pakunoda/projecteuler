import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import scala.math._

object Problem {
  def main(args: Array[String]) {
    val t1 = System.currentTimeMillis()
    val cache = new HashMap[Int, Int]

    for(x <- 1 until 10000) {
      val limit = sqrt(x).toInt
      var divisors = Vector.empty[Int]
      var count = 2

      while(count <= limit) {
        if(x % count == 0) divisors = divisors :+ count
        count += 1
      }
      var newDivisors = Vector(1)
      for(i <- divisors) {
        newDivisors = newDivisors :+ i
        if (x % 2 == 0) newDivisors = newDivisors :+ (x / i)
      }
      cache += (x -> newDivisors.distinct.sum)
    }

    println((for((x,y) <- cache if cache.contains(y) && cache(y) == x && x != y) yield x).sum)
    val t2 = System.currentTimeMillis()
    println("Time in milliseconds - " + (t2 - t1))
  }
}
