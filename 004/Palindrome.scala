import scala.collection.mutable.StringBuilder

object Palindrome {
  def main(args: Array[String]) = {
    println((for {
      i <- 1000 until 0 by -1
      j <- 1000 until 0 by -1
    } yield (i, j)).filter(x => x._1 * x._2).fold((0, 0))((x,y) => if(y._1 * y._2 > x._1 * x._2) y else x))
  }

  implicit def isPalindrome(num: Int): Boolean = {
    val str = num.toString
    str == str.reverse
  }
}
