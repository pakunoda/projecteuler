import scala.io.Source._

case class Puzzle(board: Array[Array[Int]]) {
  val isSolved = board.forall(_.forall(_!=0))
  val firstZero = (0 to 9).map(i => (i, board(i).indexOf(0))).filter(_._2 != -1).headOption
  val isValid = {
    val blockCheck = for {
      row <- 0 to 6 by 3
      column <- 0 to 6 by 3
    } yield {
      val block = (for {
        i <- row to row + 2
        j <- column to column + 2
      } yield board(i)(j)).filter(_ != 0)
      block.distinct.size == block.size
    }
    board.forall(check) && (1 to 9).map(vertical).forall(check) && blockCheck.forall(x => x)
  }

  override def toString = board.map(_.mkString(" ")).mkString("\n")
  def vertical(n: Int) = board.map(_.drop(n).take(1)).flatten
  def check(block: Array[Int]) = {
    val b = block.filter(_!=0)
    b.distinct.size == b.size
  }
  def solution: Option[Puzzle] = {
    if (isValid && isSolved) Some(this)
    else if (!isValid) None
    else firstZero match {
      case Some((y, x)) => {
        val solutions = (1 to 9).map { i =>
          if (!board(y).contains(i) && !vertical(x).contains(i)) {
            val copiedBoard = (for (b <- board) yield (for (r <- b) yield r).toArray).toArray
            copiedBoard(y)(x) = i
            Puzzle(copiedBoard).solution
          }
          else None
        }
        solutions.flatten.headOption
      }
      case _ => None
    }
  }
}

object Problem {

  def main(arg: Array[String]) {
    val numbers = for (puzzle <- extractPuzzles(fromFile("sudoku.txt").getLines.toList))
      yield puzzle.solution.map(_.board.head.take(3).mkString(""))
    println(s"solution is ${numbers.map(_.get.toInt).sum}")
  }

  def extractPuzzles(file: List[String]): List[Puzzle] = file match {
    case (x :: xs) => {
      val (puzzle, rest) = xs.splitAt(9)
      Puzzle(puzzle.map(_.map(_.toInt - 48).toArray).toArray) :: extractPuzzles(rest)
    }
    case Nil => Nil
  }

}
