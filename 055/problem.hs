main = print . length . filter (\x -> isLychrel x) $ [1..10000]

isLychrel num = isLychrel' num 0
isLychrel' num count
  | count >= 50 = True
  | isPalindrome computedNumber = False
  | otherwise = isLychrel' computedNumber (count + 1)
  where isPalindrome x = show x == (show . reverseNumber $ x)
        computedNumber = num + reverseNumber num
        reverseNumber = read . reverse . show
