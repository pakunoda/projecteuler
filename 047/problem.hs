import Data.List
import Control.Monad.ST
import Data.Array.ST
import Data.Array.Unboxed
import Control.Monad
import Debug.Trace

main = print $ take4Consec [] $ [4..] >>= \x -> [(x, hasFourFactors x)]
  where p = [2..1000000] >>= \x -> if pi ! x == 1 then [x] else []
        pi = prime 1000000
        isIntDiv x y = floor a == ceiling a where a = fromIntegral x / fromIntegral y
        intDiv x y = floor $ (fromIntegral x) / (fromIntegral y)
        hasFourFactors x
          | pi ! x == 1 = False
          | otherwise = hasFourFactors' p [] x
        hasFourFactors'  [] i j = length i == 4
        hasFourFactors' (x:xs) i j
          | pi ! j == 1 = length i == 3
          | x > intDiv j 2 = length i == 4
          | j == 1 = length i == 4
          | length i > 4 = False
          | isIntDiv j x && isIntDiv (intDiv j x) x = hasFourFactors' (x:xs) i k
          | isIntDiv j x = hasFourFactors' (x:xs) (x : i) k
          | otherwise = hasFourFactors' xs i j
          where k = intDiv j x
        take4Consec [] (x:xs) = if snd x then take4Consec [x] xs else take4Consec [] xs
        take4Consec y (x:xs)
          | fst x > 134048 = x
          | length y == 4 = head $ reverse y
          | not $ snd x = take4Consec [] xs
          | snd x = take4Consec (x:y) xs

prime :: Int -> UArray Int Int
prime n = runSTUArray $ do
  arr <- newArray ( 2 , n ) 1 :: ST s ( STUArray s Int Int )
  forM_ ( takeWhile ( \x -> x*x <= n ) [ 2 .. n ] ) $ \i -> do
    ai <- readArray arr i
    when ( ai == 1 ) $ forM_ [ i^2 , i^2 + i .. n ] $ \j -> do
      writeArray arr j 0

  return arr
