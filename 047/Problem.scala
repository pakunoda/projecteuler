import scala.collection.mutable.{HashMap, ArrayBuffer}
import scala.math._

object Problem {
  def main(args: Array[String]) {
    val primes = calculate(1000000)
    val p = primes.zipWithIndex.toMap
    val cache = new HashMap[Int, Boolean] // UGH
    var intermediate = new ArrayBuffer[Int]
    var isNotFound = true
    var i = 0

    def lookup(i: Int) = {
      if (p.contains(i)) false
      else {
        var temp = i
        cache.get(i).getOrElse {
          intermediate.clear()
          var j = 0
          var k = 0
          while(k < i / 2 && intermediate.size < 4 && temp != 1) {
            k = primes(j)
            if(temp % k == 0) intermediate += k
            while(temp % k == 0) temp = temp / k
            j += 1
          }
          cache += i -> (intermediate.size == 4)
          cache(i)
        }
      }
    }

    while(isNotFound) {
      val result = (i to i + 3).view.map(x => (x, lookup(x))).reverse
      if(result.map(_._2).reduce(_&&_)) {
        println(i)
        isNotFound = false
      }
      else i + (result.dropWhile(x => x._2).head._1 - i)
      i += 1
    }
  }

  def calculate(n: Int): ArrayBuffer[Int] = {
    val x = n + 1
    val sieve = new ArrayBuffer[Int]
    sieve += (0, 0)
    (2 until x).foreach(sieve += _)
    for(i <- 2 until(pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i) != 0) for(j <- pow(i, 2).toInt until x by i) sieve(j) = 0
    }
    sieve.filter(y => y != 0 && y < x)
  }
}
