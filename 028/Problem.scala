object Problem {
  def main(args: Array[String]) {
    println(
      (2 to 1000 by 2).foldLeft((1, 1)) { (x, y) =>
        val t = (1 to 4).map(z => z * y + x._2)
        (t.sum + x._1, t.last)
      }
    )
  }
}
