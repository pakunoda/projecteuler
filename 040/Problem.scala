import scala.collection.mutable.StringBuilder

object Problem {
  def main(args: Array[String]) {
    val builder = new StringBuilder
    def f(x: Char): Int = x.toString.toInt
    var count = 1
    while(builder.size <= 1000000) { builder.append(count); count += 1 }
    val result = builder.result
    println(f(result(0)) * f(result(9)) * f(result(99)) * f(result(999)) * f(result(9999)) * f(result(99999)) * f(result(999999)))
  }
}
