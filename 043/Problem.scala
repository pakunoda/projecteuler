val values = Array(0, 2, 3, 5, 7, 11, 13, 17)
def f(a: String, i: Int): Boolean = {
  if(i >= 8) true
  else {
    if(a.drop(i).take(3).toInt % values(i) == 0) f(a, i + 1)
    else false
  }
}
def g(x: List[Int]) = x.foldLeft("")((a, b) => a + b.toString)
println(List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).permutations.map(x => g(x)).filter(x => x.head != '0' && f(x, 1)).map(x => BigInt(x)).sum)
