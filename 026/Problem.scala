import scala.collection.mutable.ArrayBuffer

object Problem {
  def main(args: Array[String]) {
    println(primes(1000).map(i => (i, (1 to 1000).view.map(BigInt(10).modPow(_, i)).zipWithIndex.find(_._1 == 1).getOrElse(0, 0)._2)).max(Ordering[Int].on[(_, Int)](_._2)))
  }

  def primes(n: BigInt) = {
    val x = n + 1

    val sieve = new ArrayBuffer[BigInt]

    sieve += (0, 0)
    var count = 2
    while(count < x) {
      sieve += count
      count += 1
    }

    for(i <- BigInt(2) until(scala.math.pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i.toInt) != 0) {
        for(j <- scala.math.pow(i.toInt, 2).toInt until x.toInt by i.toInt) sieve(j.toInt) = 0
      }
    }
    sieve.filter(y => y != 0 && y < x)
  }
}
