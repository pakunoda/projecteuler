import Data.List
import Data.Function

main = print $ maximum [z|a <- [1..9999], let z = pandigitize "" a 1, z > 0]

pandigitize x y n
  | length x > 9 = 0
  | sort x == "123456789" = read x
  | otherwise = pandigitize (x ++ show (y * n)) y (n + 1)
