object Problem {
  def main(args: Array[String]) {
    (BigInt(1) << 1000).toString.map(_.toString.toInt).sum
  }
}
