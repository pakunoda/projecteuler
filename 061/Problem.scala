object Problem {

  val triangles = {
    def loop(n: Int): Stream[Int] = (n * (n + 1) / 2) #:: loop(n + 1)
    loop(1)
  }

  val squares = {
    def loop(n: Int): Stream[Int] = (n * n) #:: loop(n + 1)
    loop(1)
  }

  val pentagonals = {
    def loop(n: Int): Stream[Int] = (n * (3 * n - 1) / 2) #:: loop(n + 1)
    loop(1)
  }

  val hexagonals = {
    def loop(n: Int): Stream[Int] = (n * (2 * n - 1)) #:: loop(n + 1)
    loop(1)
  }

  val heptagonals = {
    def loop(n: Int): Stream[Int] = (n * (5 * n - 3) / 2) #:: loop(n + 1)
    loop(1)
  }

  val octagonals = {
    def loop(n: Int): Stream[Int] = (n * (3 * n - 2)) #:: loop(n + 1)
    loop(1)
  }

  implicit class Extension(n: Stream[Int]) extends AnyRef {
    def toKeyMap = n.takeWhile(_ < 10000).filter(_ > 999).map(x => (x.toString, true)).toMap
  }

  def main(args: Array[String]) {
    val relevantTriangles = triangles.toKeyMap.map(_._1)
    val relevantSquares = squares.toKeyMap
    val relevantPentagonals = pentagonals.toKeyMap
    val relevantHexagonals = hexagonals.toKeyMap
    val relevantHeptagonals = heptagonals.toKeyMap
    val relevantOctagonals = octagonals.toKeyMap

    val combinedKeys = (relevantSquares.map(_._1) ++
      relevantPentagonals.map(_._1) ++
      relevantHexagonals.map(_._1) ++
      relevantHeptagonals.map(_._1) ++
      relevantOctagonals.map(_._1)).toList.distinct

    val conditions = List(relevantSquares, relevantPentagonals, relevantHexagonals, relevantHeptagonals, relevantOctagonals)

    for {
      triangle <- relevantTriangles
      second <- combinedKeys.filter(_.startsWith(triangle.substring(2)))
      third <- combinedKeys.filter(_.startsWith(second.substring(2)))
      fourth <- combinedKeys.filter(_.startsWith(third.substring(2)))
      fifth <- combinedKeys.filter(_.startsWith(fourth.substring(2)))
      sixth <- combinedKeys.filter(_.startsWith(fifth.substring(2)))
      if triangle.startsWith(sixth.substring(2)) 
    } yield {
      val results = List(second, third, fourth, fifth, sixth).distinct
      if (results.size == 5 && !results.contains(triangle)) {
        if (conditions.forall(condition => results.map(condition.contains).foldLeft(false)(_||_))) {
          val yay = triangle :: results
          println(yay.mkString(","))
          println(yay.map(_.toInt).sum)
        }
      }
    }
  }
}
