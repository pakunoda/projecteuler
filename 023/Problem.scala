import scala.math._

object Problem {

  def main(args: Array[String]) {
    val t1 = System.currentTimeMillis()
    var abundantNumbers = Vector[Int]()

    for(x <- 12 to 28123) {
      val limit = sqrt(x).toInt
      var divisors = Vector.empty[Int]
      var count = 2

      while(count <= limit) {
        if(x % count == 0) divisors = divisors :+ count
        count += 1
      }
      var newDivisors = Vector(1)
      for(i <- divisors) {
        newDivisors = newDivisors :+ i
        if (x % i == 0) newDivisors = newDivisors :+ (x / i)
      }
      if(newDivisors.distinct.sum > x) abundantNumbers = abundantNumbers :+ x
    }

    val sieve = Array.range(0, 28124, 1)
    for (i <- abundantNumbers; j <- abundantNumbers) if(i + j <= 28123) sieve(i + j) = 0
    val t2 = System.currentTimeMillis()
    println("Time in millis " + (t2 - t1))
    println(sieve.sum)
  }

}
