object Project {
  def main(args: Array[String]) {
    def lookup = Array(1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880)

    val sum = (10 until 999999).map { x =>
      val firstNumber = x.toString.apply(0).toString.toInt
      if(x >= lookup(firstNumber)) {
        if(x.toString.map(y => lookup(y.toString.toInt)).sum == x) x else 0
      } else 0
    }.sum

    println(sum)
  }
}
