import scala.math._

object Problem {
  def main(args: Array[String]) {
    val t1 = System.currentTimeMillis()
    var currentTriangleCount = 1
    var currentTriangle = 1
    var isNotFound = true
    while(isNotFound) {
      currentTriangle += currentTriangleCount + 1
      currentTriangleCount += 1

      val limit = sqrt(currentTriangle)
      var bodyCount = 0
      var count = 2

      while(count <= limit) {
        if(currentTriangle % count == 0) bodyCount += 1
          count += 1
      }
      isNotFound = bodyCount * 2 + 2 <= 500
    }
    val t2 = System.currentTimeMillis()

    println("Time in milliseconds - " + (t2 - t1))
    println(currentTriangle)
  }
}
