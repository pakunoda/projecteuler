object Problem {
  def main(args: Array[String]) {
    val games = scala.io.Source.fromFile("poker.txt").getLines().map(line => (new Hand(line.take(14)), new Hand(line.drop(15))))
    println(games.filter(hands => hands._1.isBetterThan(hands._2)).size)
  }
}

class Hand(unparsedCards: String) {
  /* Variable declarations */
  val cards = unparsedCards.split(' ').map(new Card(_))
  val suits = cards.map(x => x.suit)
  val values = cards.map(x => x.value).toList.sortBy(x => x)
  private val distinctValuesSize = values.distinct.size
  private val firstCardCount = values.filter(values.head == _).size
  private val threeOfAKindHinting = values.map(x => values.filter(_ == x).size == 3).reduce(_||_)

  /* Hand checks */
  val isTwoOfAKind = distinctValuesSize == 4
  val isTwoPairs = distinctValuesSize == 3 && !threeOfAKindHinting
  val isThreeOfAKind = distinctValuesSize == 3 && threeOfAKindHinting
  val isFourOfAKind = distinctValuesSize == 2 && (firstCardCount == 4 || firstCardCount == 1)
  val isFullHouse = distinctValuesSize == 2 && (firstCardCount == 2 || firstCardCount == 3)
  val isStraight = (values.head until values.head + 5).intersect(values).size == 5
  val isFlush = suits.distinct.size == 1
  val isStraightFlush = isStraight && isFlush
  val isRoyalFlush = isStraight && isFlush && values.head == 10 && suits.head == Suit.Spade
  private val ranks = List(isRoyalFlush, isStraightFlush, isFourOfAKind, isFullHouse, isFlush, isStraight, isThreeOfAKind, isTwoPairs, isTwoOfAKind, true).zip(9 to 0 by -1)
  val rank = ranks.dropWhile(!_._1).head._2

  def isBetterThan(hand: Hand) = {
    if (rank != hand.rank) rank > hand.rank
    else if (isStraight || rank == 0) this.hasHigherCardThan(hand)
    else {
      val highScore = values.reduce((x,y) => if(values.filter(_ == y).size > 1 && x < y) y else x)
      val opposingHighestScore = hand.values.reduce((x,y) => if(hand.values.filter(_ == y).size > 1 && x < y) y else x)
      if (highScore == opposingHighestScore) this.hasHigherCardThan(hand)
      else highScore > opposingHighestScore
    }
  }

  def hasHigherCardThan(hand: Hand) = values.diff(hand.values).last > hand.values.diff(values).last
}

class Card(unparsedCard: String) {
  val highCards = Map('T' -> 10, 'J' -> 11, 'Q' -> 12, 'K' -> 13, 'A' -> 14)
  val suit = Suit.withName(unparsedCard(1).toString)
  val value = highCards.getOrElse(unparsedCard(0), unparsedCard(0).toString.toInt)
}

object Suit extends Enumeration ("D", "S", "C", "H") {
  type Suit = Value
  val Diamond, Spade, Club, Heart = Value
}
