import org.pakunoda.utilities.Memoize

object Problem {

  def main(args: Array[String]) {
    val t1 = System.currentTimeMillis()
    val result = findPaths(20, 20)
    val t2 = System.currentTimeMillis()
    println("result is " + result)
    println("time " + (t2 - t1))
  }

  val memoizedFindPaths = Memoize((x: BigInt, y: BigInt) => findPaths((x, y)))
  def findPaths(x:(BigInt, BigInt)): BigInt = {
    x match {
      case (x, _) if x == 0 => 1
      case (_, y) if y == 0 => 1
      case (x, y) => memoizedFindPaths(x - 1, y) + memoizedFindPaths(x, y - 1)
    }
  }
}
