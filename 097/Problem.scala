object Problem {
  def main(args: Array[String]) {
    println((BigInt(2).pow(7830457) * 28433 + 1) % BigInt("10000000000"))
  }
}
