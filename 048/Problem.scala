import scala.math._

object Problem {
  def main(args: Array[String]) {
    println((1 to 1000).map(i => BigInt(i).pow(i)).sum.toString.takeRight(10))
  }
}
