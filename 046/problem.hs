import qualified Data.Map as M

main = print . head . dropWhile (\x -> f x) $ [33,35..]
  where pIndexed = M.fromList $ zip p [1..]
        p = primes 10000
        f x = if M.member x pIndexed then True
              else (length . dropWhile (\y -> g x y) . filter (< x) $ p) > 0
        g x y = isNotInteger . sqrt $ ((fromIntegral(x) - fromIntegral(y)) / 2)
        isNotInteger x = floor x /= ceiling x

primes m = 2 : sieve [3,5..m]
  where sieve (p:xs)
          | p*p > m = p : xs
          | otherwise = p : sieve [x | x <- xs, rem x p /= 0]
