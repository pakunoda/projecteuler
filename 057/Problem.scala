object Problem {
  def main(args: Array[String]) {
    var result = 0
    var nominator = BigInt(1)
    var denominator = BigInt(2)

    (0 to 999).map { _ =>
      var newNominator = denominator * 2
      var newDenominator = (nominator * 2) + (newNominator * 2)

      if (denominator % 2 == 0) {
        newNominator = denominator
        newDenominator = nominator + (denominator * 2)
      }

      val (a, b) = reduceFraction(newNominator + newDenominator, newDenominator)
      if (a.toString.size > b.toString.size) result += 1

      nominator = newNominator
      denominator = newDenominator
    }
    println(result)
  }

  def reduceFraction(a: BigInt, b: BigInt): (BigInt, BigInt) = a.gcd(b) match {
    case gcd if gcd == 1 => (a, b)
    case gcd => reduceFraction(a / gcd, b / gcd)
  }
}
