import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.collection.immutable.VectorBuilder
import org.jscience.mathematics.number.LargeInteger

object Problem {
  def main(args: Array[String]) {
    println(t5(exp(10, 3)))
  }

  def t5(x: BigInt): BigInt = {
    println("initial count " + x)
    var count = x
    var numberOfInts = 0
    while(count > 0) {
      println("Count in loop " + count)
      if(count > Int.MaxValue) {
        (BigInt(0) until Int.MaxValue).par.view.foreach(x => if(f5(factorial((2 * count) - 1)) < (2 * f5(factorial(count)))) numberOfInts += 1)
        count -= Int.MaxValue
      }
      else {
        (BigInt(1) until count).par.foreach{x =>
          if(x % 10 == 0) println("count at " + x)
          if(f5(factorial((2 * x) - 1)) < (2 * f5(factorial(x)))) numberOfInts += 1
        }
        count = 0
      }
    }
    numberOfInts
  }

  def f5(x: BigInt): BigInt = {
    if(x < 5) 0
    else f5(x, 1, BigInt(1))
  }

  @tailrec def f5(x: BigInt, y: BigInt, count: BigInt): BigInt = {
    val z = exp(5, count)
    if(z > x) y
    else if(x % z == 0) f5(x, count, count + 1)
    else f5(x, y, count + 1)
  }

  def factorial(x: BigInt) = {
    def loop(seq: Seq[LargeInteger]): LargeInteger = seq.length match {
      case 0 => throw new IllegalArgumentException
      case 1 => seq.head
      case _ => loop {
        val (a, b) = seq.splitAt(seq.length / 2)
        a.zipAll(b, LargeInteger.ONE, LargeInteger.ONE).map(i => i._1 times i._2)
      }
    }
    BigInt(loop((1 to x.toInt).map(LargeInteger.valueOf(_)).toIndexedSeq).toString)
  }

  def exp(x: BigInt, y: BigInt): BigInt = if(y == 0) BigInt(1) else (BigInt(1) to y).fold(BigInt(1))((z,y) => z*x)
}
