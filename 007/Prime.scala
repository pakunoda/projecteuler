object Prime {
  def main(args: Array[String]) {
    var list: List[BigInt] = Nil
    var count = BigInt(2)

    while(list.size <= 10000) {
      if(isPrime(count)) list = count :: list
      count += 1
    }
    println(list.head)
  }

  def isPrime(num: BigInt): Boolean = {
    if ((num % 2 == 0 && num > 2) || num <= 1) false
    else isPrime(num, 3)
  }

  def isPrime(num: BigInt, count: BigInt): Boolean = {
    if(count > num / 2) true
    else if(num % count == 0) false
    else isPrime(num, count + 2)
  }
}
