package org.pakunoda.utilities
import scala.collection.mutable.HashMap

object Memoize {

  class Memoize1[-T, +R](f: (T) => R) extends (T => R) {
    private[this] val cache = new HashMap[T, R]

    def apply(x: T): R = {
      if (cache.contains(x)) cache(x)
      else {
        val result = f(x)
        cache += (x -> result)
        result
      }
    }
  }

  class Memoize2[-T, -U, +R](f: (T, U) => R) extends ((T, U) => R) {
    private[this] val cache = new HashMap[(T, U), R]

    def apply(x: T, y: U): R = {
      val key = (x, y)
      if (cache.contains(key)) cache(key)
        else {
        val result = f(x, y)
        cache += ((x, y) -> result)
        result
      }
    }
  }

  class Memoize3[-T, -U, -V, +R](f: (T, U, V) => R) extends ((T, U, V) => R) {
    private[this] val cache = new HashMap[(T, U, V), R]

    def apply(x: T, y: U, z: V): R = {
      val key = (x, y, z)
      if (cache.contains(key)) cache(key)
        else {
        val result = f(x, y, z)
        cache += ((x, y, z) -> result)
        result
      }
    }
  }

  def apply[T, R](f: T => R) = new Memoize1(f)
  def apply[T, U, R](f: (T, U) => R) = new Memoize2(f)
  def apply[T, U, V, R](f: (T, U, V) => R) = new Memoize3(f)
}
