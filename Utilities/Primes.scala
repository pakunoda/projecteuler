package org.pakunoda.Utilities

object Primes {

  def calculate(n: BigInt): ArrayBuffer[BigInt] = {
    val x = n + 1

    val sieve = new ArrayBuffer[BigInt]
    sieve += (0, 0)
    for(i <- 2 until x.toInt) sieve += i

    for(i <- BigInt(2) until(scala.math.pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i.toInt) != 0) {
        for(j <- scala.math.pow(i.toInt, 2).toInt until x.toInt by i.toInt) sieve(j.toInt) = 0
      }
    }
    sieve.filter(y => y != 0 && y < x)
  }

}
