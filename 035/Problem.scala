import scala.collection.mutable.ArrayBuffer
import scala.collection.immutable.HashMap

object Problem {

  def main(args: Array[String]) {
    val filteredSieve = primes(1000000).foldLeft(HashMap.empty[BigInt, Boolean])((x, y) => x + (y -> true))
    for((prime, _) <- filteredSieve) {
      if (!circularPrimes.contains(prime)) {
        var primeAsString = prime.toString
        var cyclicNumbers = new ArrayBuffer[BigInt]

        for(i <- 0 until primeAsString.size) {
          primeAsString = primeAsString.drop(1) + primeAsString.take(1)
          cyclicNumbers += BigInt(primeAsString)
        }

        if(cyclicNumbers.map(filteredSieve.contains(_)).reduce(_&&_)) circularPrimes ++= cyclicNumbers
      }
    }
    println(circularPrimes.distinct.size)
  }

  def primes(n: BigInt): ArrayBuffer[BigInt] = {
    val x = n + 1

    if(sieve.size == 0) {
      sieve += (0, 0)
      for(i <- 2 until x.toInt) sieve += i
    }
    else if(sieve.size < x) {
      for(i <- sieve.size until x.toInt) sieve += i
    }

    for(i <- BigInt(2) until(scala.math.pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i.toInt) != 0) {
        for(j <- scala.math.pow(i.toInt, 2).toInt until x.toInt by i.toInt) sieve(j.toInt) = 0
      }
    }
    sieve.filter(y => y != 0 && y < x)
  }

  val sieve = new ArrayBuffer[BigInt]
  val circularPrimes = new ArrayBuffer[BigInt]
}
