import scala.math._

object Problem {
  val naturalNumbers = {
    def loop(n: Int): Stream[Int] = n #:: loop(n + 1)
    loop(1)
  }

  def getExpSize(n: Int, e: Int) = BigDecimal(n).pow(e).toString.size

  def main(args: Array[String]) {
    println((for {
      base <- 1 to 9
      exp <- naturalNumbers.dropWhile(x => getExpSize(base, x) < x).takeWhile(x => getExpSize(base, x) == x)
    } yield exp).size)
  }
}
