import Data.List
import Control.Monad.ST
import Data.Array.ST
import Data.Array.Unboxed
import Control.Monad
import Data.Ord

main = print . take 1 . reverse . sortBy (comparing snd) $ primes >>= \x -> f x
  where p = prime 1000000
        primes = filter (\x -> p ! x == 0) [2..1000000]
        sq x = floor . sqrt . fromIntegral $ x
        f x = f' x primes 0
        f' x [] result = [(x, result)]
        f' x (y:ys) z
          | null ys || y > sq x || x <= y + head ys = f' x [] 0
          | otherwise = if len > z then f' x [] len else f' x ys z
          where len = g x 0 0 (y:ys)
        g x y z [] = 0
        g target tempSum count (z:zs)
          | z + tempSum > target = 0
          | z + tempSum == target = count + 1
          | otherwise = g target (tempSum + z) (count + 1) zs

prime :: Int -> UArray Int Int
prime n = runSTUArray $ do
  arr <- newArray ( 2 , n ) 1 :: ST s ( STUArray s Int Int )
  forM_ ( takeWhile ( \x -> x*x <= n ) [ 2 .. n ] ) $ \i -> do
    ai <- readArray arr i
    when ( ai == 1 ) $ forM_ [ i^2 , i^2 + i .. n ] $ \j -> do
      writeArray arr j 0

  return arr
