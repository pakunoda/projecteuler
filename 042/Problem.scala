import scala.io._
val t = (1 to 1000).foldLeft(Map[Int, Boolean]())((x,y) => x + ((.5 * y * (y + 1)).toInt -> true))
val words = Source.fromFile("words.txt").getLines().toList.head.split(',').map(_.replaceAllLiterally("\"",""))
println(words.map(x => x.map(_.toInt - 64).sum).filter(x => t.contains(x)).size)
