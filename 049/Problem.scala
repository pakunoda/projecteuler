import scala.collection.mutable.{HashMap, ArrayBuffer}
import scala.math._

object Problem {
  def main(args: Array[String]) {
    val p = calculate(9999)
    val primes = p.filter(x => x != 0 && x > 999 && x <= 9999)
    println(primes.map { x =>
      primes.intersect(x.toString.permutations.map(x => x.toInt).toList)
    }.filter(x => x.size > 2 && x.head < 5000).map{x => x.map(y => y - x.head) }.map(x => x.map{ y =>
      if (x.filter(j => y == j).size > 1) y else 0
    }.sum).filter(_>0))
  }

  def calculate(n: Int): ArrayBuffer[Int] = {
    val x = n + 1
    val sieve = new ArrayBuffer[Int]
    sieve += (0, 0)
    (2 until x).foreach(sieve += _)
    for(i <- 2 until(pow(n.toDouble, 0.5).toInt + 1)) {
      if(sieve(i) != 0) for(j <- pow(i, 2).toInt until x by i) sieve(j) = 0
    }
    sieve
  }
}
