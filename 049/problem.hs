import Data.List
import Control.Monad.ST
import Data.Array.ST
import Data.Array.Unboxed
import Control.Monad

main = print $ take 4 $ primes >>= \x -> f . inter . map (\x -> read x) . permutations . show $ x
  where p = prime 10000
        primes = [2..10000] >>= \x -> if p ! x == 0 || x < 1000 || x >= 10000 then [] else [x]
        f [] = []
        f (1487:xs) = []
        f (x:xs) = if (length . fil . sub $ xs) > 0 then (x:xs) else []
          where sub = map (\y -> y - x)
                fil = filter (\y -> y /= 0 && (y + x `elem` xs) && (y * 2 + x `elem` xs) && (isPrime (x + y) && (isPrime (x + y * 2))))
                isPrime x = p ! x == 1
        inter x = x `intersect` primes

prime :: Int -> UArray Int Int
prime n = runSTUArray $ do
  arr <- newArray ( 2 , n ) 1 :: ST s ( STUArray s Int Int )
  forM_ ( takeWhile ( \x -> x*x <= n ) [ 2 .. n ] ) $ \i -> do
    ai <- readArray arr i
    when ( ai == 1 ) $ forM_ [ i^2 , i^2 + i .. n ] $ \j -> do
      writeArray arr j 0

  return arr
