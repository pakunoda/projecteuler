import scala.io._

object Problem {

  def main(args: Array[String]) {
    println((1 until 1000).map(x => g(x + "").size).sum + "onethousand".size)
  }

  def g(x: String): String = {
    if(x.size == 1) words(x.toInt)
    else if (words.contains(x.toInt)) words(x.toInt)
    else if (x.size == 3) words(x(0).toString.toInt) + (if(x.toInt % 100 == 0) "hundred" else "hundredand") + g(x.takeRight(2))
    else words((x.head.toString + "0").toInt) + words(x.last.toString.toInt)
  }

  val words = Map(0 -> "", 1 -> "one", 2 -> "two", 3 -> "three", 4 -> "four", 5 -> "five",
    6 -> "six", 7 -> "seven", 8 -> "eight", 9 -> "nine", 10 -> "ten",
    11 -> "eleven", 12 -> "twelve", 13 -> "thirteen", 14 -> "fourteen", 15 -> "fifteen",
    16 -> "sixteen", 17 -> "seventeen", 18 -> "eighteen", 19 -> "nineteen", 20 -> "twenty",
    30 -> "thirty", 40 -> "forty", 50 -> "fifty", 60 -> "sixty", 70 -> "seventy",
    80 -> "eighty", 90 -> "ninety")

}
