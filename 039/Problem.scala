import scala.math._

object Problem {
  def main(args: Array[String]) {
    val t = (120 until 1000).par.map(quad(_))
    println(t.map(x => (x.headOption, x.size)).toList.sortBy(_._2).last)
  }

  def quad(number: Int) = {
    for {
      a <- (sqrt(number).toInt until number / 2)
      b <- (a until number / 2)
      c = sqrt((a*a) + (b*b))
      if(a + b + c == number)
    } yield (a, b, c)
  }
}
