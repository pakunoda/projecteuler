#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int bool;
const bool TRUE = 1;
const bool FALSE = 0;

unsigned long * calculate(long n);
bool is_prime(unsigned long p1, unsigned long p2);

unsigned long * prime_lookup;
unsigned long * primes;
size_t primes_size;

int main(int argc, char ** argv) {
  long ceiling = 100000000;
  long limit = 10000;
  prime_lookup = calculate(ceiling);
  int i = 0, j = 0, k = 0, l = 0, m = 0;

  for (i = 0; i < limit; i++) {
    if (prime_lookup[i] && i != 2 && i != 5) {
      primes = realloc(primes, sizeof(unsigned long) * (primes_size + 1));
      primes[primes_size++] = i;
    }
  }

  for (i = 0; i < primes_size; i++) {
    for (j = i; j < primes_size; j++) {
      if (is_prime(primes[i], primes[j])) {
        for (k = j; k < primes_size; k++) {
          if (is_prime(primes[i], primes[k]) && is_prime(primes[j], primes[k])) {
            for (l = k; l < primes_size; l++) {
              if (is_prime(primes[i], primes[l]) && is_prime(primes[j], primes[l]) && is_prime(primes[k], primes[l])) {
                for (m = l; m < primes_size; m++) {
                  if (is_prime(primes[i], primes[m]) && is_prime(primes[j], primes[m]) && is_prime(primes[k], primes[m]) && is_prime(primes[l], primes[m])) {
                    unsigned long p1 = primes[i], p2 = primes[j], p3 = primes[k], p4 = primes[l], p5 = primes[m];
                    printf("solution - %lu %lu %lu %lu %lu = %lu\n", p1, p2, p3, p4, p5, p1 + p2 + p3 + p4 + p5);
                    free(primes);
                    free(prime_lookup);
                    return -1;
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  free(primes);
  free(prime_lookup);
  return -1;
}

unsigned long * calculate(long n) {
  /* Init array */
  unsigned long x = n + 1;
  unsigned long * sieve = calloc(x, sizeof(long));
  assert(sieve);
  for (long a = 0; a < n; a++)
    sieve[a] = 1;

  /* Preset array */

  sieve[0] = sieve[1] = 0;

  long i = 2;
  while (n / i / 1.0 > 2) {
    if (sieve[i] != 0)
      for (long j = i * i; j < x; j += i)
        sieve[j] = 0;
    i++;
  }
  return sieve;
}

bool is_prime(unsigned long p1, unsigned long p2) {
  char buffer[50];
  unsigned long n = sprintf(buffer, "%lu%lu", p1, p2);
  assert(n >= 1);
  unsigned long first_number = atol(buffer);
  n = sprintf(buffer, "%lu%lu", p2, p1);
  assert(n >= 1);
  unsigned long second_number = atol(buffer);
  return *(prime_lookup + first_number) && *(prime_lookup + second_number);
}
