import scala.math._

object Problem {

  def isPrimePair(numbers: Int *): Boolean = {
    val (init, last) = numbers.map(Primes.primes).splitAt(numbers.size - 1)
    init.forall(Primes.isPrimePair(last.head, _))
  }

  def main(args: Array[String]) {
    try {
      for {
        i <- 0 until Primes.primes.size
        j <- i until Primes.primes.size if isPrimePair(i, j)
        k <- j until Primes.primes.size if isPrimePair(i, j, k)
        l <- k until Primes.primes.size if isPrimePair(i, j, k, l)
        m <- l until Primes.primes.size if isPrimePair(i, j, k, l, m)
      } yield {
        val solution = List(i, j, k, l, m).map(Primes.primes)
        println(solution.mkString(", ") + " and total is " + solution.sum)
        throw new Exception()
      }
    }
    catch {
      case _: Throwable => ()
    }
  }

}

object Primes {
  val primeLookup = calculate(99999999)
  val primes = primeLookup.take(10000).filter(x =>x != 0 && x != 2 && x != 5)
  def isPrimePair(x: Int, y: Int) = primeLookup(("" + x + y).toInt) != 0 && primeLookup(("" + y + x).toInt) != 0
  private def calculate(n: Int): Array[Int] = {
    val x = n + 1
    val sieve = Array.range(0, x)
    sieve(0) = 0
    sieve(1) = 0
    var i = 2
    while (i < pow(n, 0.5).toInt + 1) {
      if (sieve(i) != 0) {
        var j = pow(i, 2).toInt
        while (j < x) {
          sieve(j) = 0
          j += i
        }
      }
      i += 1
    }
    sieve
  }
}

