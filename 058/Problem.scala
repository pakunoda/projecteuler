import scala.math._
object Problem {
  def main(args: Array[String]) {
    var iteration = 0
    var totalNumbers = 0
    var primesFound = 0
    var start = 1

    while(primesFound == 0 || primesFound / (totalNumbers + 0.0) > .1) {
      iteration += 2
      totalNumbers += 4
      (1 to 4).map {x =>
        start += iteration
        if (isPrime(start)) primesFound += 1
      }
    }
    println(iteration + 1)
  }

  def isPrime(i: Int): Boolean = {
    def inner(a: Int, b: Int, c: Int): Boolean = {
      if(a > b) true
      else if (c % a == 0) false
      else inner(a + 2, b, c)
    }
    if(i % 2 == 0) false
    else inner(3, sqrt(i).toInt, i)
  }
}
