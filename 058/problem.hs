main = print $ spiral 1 0 0 0

spiral count iteration numberOfPrimes totalNumbers
  | isBelowTarget = iteration + 1
  | otherwise = spiral next (iteration + 2) primesCount (totalNumbers + 4)
  where next = count + iteration * 4
        primesCount = numberOfPrimes + (length . filter isPrime . map (\x -> count + x * iteration) $ [4,3..1])
        isBelowTarget = numberOfPrimes /= 0 && ((fromIntegral primesCount) / (fromIntegral (totalNumbers + 4))) < 0.1

isPrime i
  | mod i 2 == 0 = False
  | otherwise =  isPrime' 3 (sq i) i

isPrime' a b c
  | a > b = True
  | mod c a == 0 = False
  | otherwise = isPrime' (a + 2) b c

sq = floor . sqrt . fromIntegral
