import scala.collection.mutable.HashMap

object Problem {
  def main(args: Array[String]) {
    val foundProducts = new HashMap[BigInt, Boolean]
    def isPandigital(a: BigInt, b: BigInt): Boolean = ((a * b).toString + a + b).sortBy(x => x) == "123456789"
    for{i <- BigInt(2) to 10000; j <- BigInt(2) to 10000 / i} {
      if(!foundProducts.contains(i * j)) if(isPandigital(i, j)) foundProducts += (i * j -> true)
    }
    println(foundProducts.map(i => i._1).sum)
  }
}
