object Problem {
  def main(args: Array[String]) {
    def isPalindrome(i: String) = i == i.reverse
    println((for(i <- BigInt(1) until 1000000 by 2 if isPalindrome(i.toString) && isPalindrome(i.toString(2))) yield i).sum)
  }
}
