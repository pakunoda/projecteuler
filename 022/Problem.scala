import scala.io._

object Problem {

  def main(args: Array[String]) {
    implicit def f(x: String): Int = x.map(_.toInt - 'A'.toInt + 1).sum
    val list = Source.fromFile("names.txt").getLines().map(x => x.replaceAllLiterally("\"", "").split(",")).flatten.toArray.sortBy(x => x)
    println((1 to list.size).map(x => list(x-1)*x).sum)
  }

}
